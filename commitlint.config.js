module.exports = {extends: ['@commitlint/config-conventional'],
"rules": {
    "subject-max-length": [0, "always", 20],
    "subject-min-length": [0, "always", 20],
  }
}